
import
  preserves, std/options

type
  IconFieldKind* {.pure.} = enum
    `present`, `absent`
  IconFieldPresent* {.preservesDictionary.} = object
    `icon`*: string
  IconFieldAbsent* {.preservesDictionary.} = object
  `IconField`* {.preservesOr.} = object
    case orKind*: IconFieldKind
    of IconFieldKind.`present`:
      `present`*: IconFieldPresent
    of IconFieldKind.`absent`:
      `absent`*: IconFieldAbsent
  AttrsBody* = Option[Value]
  AttrsCategory* = Option[string]
  AttrsIcon* = Option[string]
  AttrsUrgency* = Option[string]
  `Attrs`* {.preservesDictionary.} = object
    `body`*: Option[Value]
    `category`*: Option[string]
    `icon`*: Option[string]
    `urgency`*: Option[string]
  UrgencyFieldKind* {.pure.} = enum
    `present`, `absent`
  UrgencyFieldPresent* {.preservesDictionary.} = object
    `urgency`*: string
  UrgencyFieldAbsent* {.preservesDictionary.} = object
  `UrgencyField`* {.preservesOr.} = object
    case orKind*: UrgencyFieldKind
    of UrgencyFieldKind.`present`:
      `present`*: UrgencyFieldPresent
    of UrgencyFieldKind.`absent`:
      `absent`*: UrgencyFieldAbsent
  ServerInfo* {.preservesRecord: "server-info".} = object
    `name`*: string
    `vendor`*: string
    `version`*: string
    `specVersion`*: string
  Notify* {.preservesRecord: "notify".} = object
    `summary`*: Value
    `attrs`*: Attrs
  CategoryFieldKind* {.pure.} = enum
    `present`, `absent`
  CategoryFieldPresent* {.preservesDictionary.} = object
    `category`*: string
  CategoryFieldAbsent* {.preservesDictionary.} = object
  `CategoryField`* {.preservesOr.} = object
    case orKind*: CategoryFieldKind
    of CategoryFieldKind.`present`:
      `present`*: CategoryFieldPresent
    of CategoryFieldKind.`absent`:
      `absent`*: CategoryFieldAbsent
  BodyFieldKind* {.pure.} = enum
    `present`, `absent`
  BodyFieldPresent* {.preservesDictionary.} = object
    `body`*: Value
  BodyFieldAbsent* {.preservesDictionary.} = object
  `BodyField`* {.preservesOr.} = object
    case orKind*: BodyFieldKind
    of BodyFieldKind.`present`:
      `present`*: BodyFieldPresent
    of BodyFieldKind.`absent`:
      `absent`*: BodyFieldAbsent
proc `$`*(x: IconField | Attrs | UrgencyField | ServerInfo | Notify |
    CategoryField |
    BodyField): string =
  `$`(toPreserves(x))

proc encode*(x: IconField | Attrs | UrgencyField | ServerInfo | Notify |
    CategoryField |
    BodyField): seq[byte] =
  encode(toPreserves(x))
