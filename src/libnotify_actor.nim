# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  pkg/syndicate,
  pkg/syndicate/[gatekeepers, relays],
  ./schema/protocol,
  ./private/pkgconfig

from std/strutils import normalize

{.passC: pkgConfig"--cflags libnotify".}
{.passL: pkgConfig"--libs libnotify".}

const notifyHeader = "<libnotify/notify.h>"
{.pragma: notify, header: notifyHeader, importc: "notify_$1".}
{.pragma: notification, header: notifyHeader, importc: "notify_notification_$1".}

type
  Notification {.importc: "NotifyNotification".} = ptr object
    discard
  GError {.importc, header: "glib/gerror.h".} = object
    message: cstring

proc g_free(mem: pointer) {.importc.}
proc g_error_free(err: ptr GError) {.importc, header: "glib/gerror.h".}

proc init(appName: cstring): bool {.notify.}

proc uninit() {.notify.}

proc is_initted: bool {.notify.}

proc get_server_info(name, vendor, version, specVersion: ptr cstring): bool {.notify.}

proc notification_new(summary, body: cstring; icon: cstring = nil): Notification {.notify.}

proc set_app_name(n: Notification; s: cstring) {.notification.}
proc set_category(n: Notification; s: cstring) {.notification.}

type NotifyUrgency {.importc.} = enum Low, Normal, Critical
proc set_urgency(n: Notification; u: NotifyUrgency) {.notification.}

proc show(n: Notification) =
  proc show(n: Notification; err: ptr (ptr GError)): bool {.notification.}
  var err: ptr GError
  if not show(n, addr err):
    doAssert not err.isNil, "GLIB errored without setting an error"
    doAssert not err.message.isNil, "GLIB errored without setting an error"
    stderr.writeLine "show failed: ", err.message
  if not err.isNil:
    g_error_free(err)

proc assertServerInfo(turn: Turn; ds: Cap) =
  var name, vendor, version, specVersion: cstring
  if get_server_info(name.addr, vendor.addr, version.addr, specVersion.addr):
    publish(turn, ds, ServerInfo(
        name: $name,
        vendor: $vendor,
        specVersion: $specVersion,
      ))
  else:
    stderr.writeLine "failed to get server info!"

type NotifyEntity = ref object of Entity

proc toString(v: Value): string =
  if v.isString: v.string
  else: $v

proc toString(opt: Option[Value]): string =
  if opt.isSome: result = opt.get.toString

proc show(n: Notify) =
  assert is_initted()
  var
    notice = notification_new(
        n.summary.toString, n.attrs.body.toString, n.attrs.icon.get(""))
    urgency = n.attrs.urgency.get("")
    category = n.attrs.category.get("")
  case urgency.normalize
  of "high", "critical":
    notice.set_urgency(Critical)
  of "low":
    notice.set_urgency(Low)
  else:
    discard
  if category != "":
    notice.set_category(category)
  notice.show()

proc m(turn: Turn, e: Entity, v: Value) =
  v{0}.preservesTo(Notify).map do (n: Notify):
    n.show()

proc a(turn: Turn, e: Entity, v: Value, h: Handle) =
  m(turn, e, v)

proc installNotifyEntity(turn: Turn; ds: Cap) =
  let cap = turn.facet.newCap Entity(facet: turn.facet, a: a, m: m)
  observe(turn, ds, Notify.matchType.grab, cap)
  assertServerInfo(turn, ds)

runActor("libnotify-actor") do (turn: Turn):
  doAssert init("libnotify-actor"), "failed to initialize libnotify"
  turn.actor.onExit do (actor: Actor): uninit()
  resolveEnvironment(turn, installNotifyEntity)
