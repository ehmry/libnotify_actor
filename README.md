# libnotify-actor

[Syndicate](https://syndicate-lang.org/) actor for sending notifications with [Libnotify](https://gnome.pages.gitlab.gnome.org/libnotify/index.html).

The notification protocol is described in [protocol.prs](./protocol.prs).

### Sample config
```
# Require notifications as an abstract service:
#
<require-service <service notifier>>

# Send error notifications when `<service notifier>` comes up:
#
? <service-object <service notifier> ?notifier> [

  ? <service-state ?service failed> [
    $notifier ! <notify $service { body: "service failed" }>
  ]

  ? <a ?req <error ?err>> [
    $notifier ! <notify "error" { body: <a $req <error $err>> }>
  ]

]

# Declare the abstract service to depend on the libnotify-actor and a graphical notifier:
#
<depends-on <service notifier> <service-state <daemon libnotify-actor> ready>>
<depends-on <daemon libnotify-actor> <service-state <wayland <daemon mako>> ready>>

# Start libnotify-actor with a known $DBUS_SESSION_BUS_ADDRESS:
#
? <wayland-env { "DBUS_SESSION_BUS_ADDRESS": ?DBUS_SESSION_BUS_ADDRESS }> [
  <daemon libnotify-actor {
    argv: "libnotify-actor"
    protocol: application/syndicate
    env: { "DBUS_SESSION_BUS_ADDRESS": $DBUS_SESSION_BUS_ADDRESS }
    readyOnStart: #f
  }>

  # Re-announce the daemon as the abstract service:
  #
  ? <service-object <daemon libnotify-actor> ?obj> [
    $obj ? <server-info ?name ?vendor ?version ?specVersion> [
      $log ! <log "libnotify" { line: <server-info $name $vendor $version $specVersion> }>
      $config += <service-state <daemon libnotify-actor> ready>
      $config += <service-object <service notifier> $obj>
    ]
  ]
]
```
