{
  pkgs ? import <nixpkgs> { },
  src ? if pkgs.lib.inNixShell then null else pkgs.lib.cleanSource ./.,
}:
with pkgs;
buildNimSbom (finalAttrs: {
  inherit src;
  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ libnotify ];
}) ./sbom.json
